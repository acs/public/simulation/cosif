% This function generates input and time vectors that are of size:
% t_size = integer x L x Ts, where L is lifted period of the system
% if the system is with original time period then the vector is simply t = 0:Ts:(t_final-Ts); 


%[t, u] = f_genTimeInputVctrs(Ts, t_final, L)

function [t, u] = f_genTimeInputVctrs(P, Ts, t_final, varargin)

% t = 0:Ts:(t_final-Ts);  
t = 0:Ts:(t_final); 
t_size = size(t,2);
if (nargin > 3)
    L = varargin{1};
    %we have lifted system -> t_size must be integer x L x Ts
    tmp = rem(t_size, L);
    if tmp ~= 0
        t_final = t_final + (L-tmp)*Ts;
%         t = 0:Ts:(t_final-Ts);  
        t = 0:Ts:(t_final); 
        t_size = size(t,2);
    end
end

u = zeros(P,t_size);
%sine wave input vector for source 1
% See paper Dynamic Phasor Model-Based Sync Est Algo... for ideas on tests!
% See also Reconstruction of transient Waveforms from Phasors at the... 

DC = 0;
A_a1= 5; %magnitude 
f1=50.0; %1000;  
 tmp1 = A_a1.*sin(2*pi*f1*t+pi/3); % +a1.*sin(4*pi*f1*t+pi/3);
 tmp1(round(t_size/8):t_size) = 0.0;
 f_a1 = 1.0; %oscillations of the magnitude (modulation of the magnitude)
 A_f_a1 = A_a1*0.1;
 a1=A_a1*(1+A_f_a1*sin(2*pi*f_a1*t+pi/3)); %modulation of amplitude with freq f_a1
 f1 = 50;
 tmp12 = a1.*sin(2*pi*f1*t+pi/3);
 tmp12(1:(round(t_size/8)-1))= 0.0;
 tmp12(round(t_size/4):t_size) = 0.0;
 tmp1 = tmp1+tmp12;

 % u(1,:)=5+a1.*sin(2*pi*f_a1*t+pi/3); % we generate signal of f_a1 freq that is not equal to fundamental 
%add another freq in the signal in magnitude 
a12 = 0.2*A_a1;
f12 = 1.0; 
% f12 = 60;
% f12 = 150;
tmp2 = a12.*sin(2*pi*f12*t+pi/4); %only signal with freq f_a1
tmp2(1:round(t_size/4)) = 0.0;
tmp2(round(2*t_size/4):t_size) = 0.0;

% tmp3=a1.*sin(2*pi*f1*t+pi/3)+a12.*sin(2*pi*f12*t+pi/4);
f1_3 = 1000.0;
A1_3 = 50;
tmp3=A1_3*sin(2*pi*f1_3*t+pi/3); % +a12.*sin(2*pi*f12*t+pi/4); %sum of signal with fundamental freq and signal with freq f_a1
tmp3(1:round(2*t_size/4)) = 0.0;
tmp3(round(3*t_size/4):t_size) = 0.0;

%tmp 4 : ramp of the amplitude
k=1.2;
a12 = A_a1/(t_final/4*3)/1.2*k*t;
tmp4=a12.*sin(2*pi*f1*t+pi/3);
tmp4(1:(round(3*t_size/4)-1)) = 0.0;
u(1,:)=tmp1 + tmp2 + tmp3 + tmp4;

% f12 = 10.0;
% tmp3=A_a1*sin(2*pi*f1*t+pi/3)+a12.*sin(2*pi*f12*t+pi/4); %sum of signal with fundamental freq and signal with freq f_a1
% u(1,:)=tmp3;

% f12 = 1.02;
% tmp4=a12.*sin(2*pi*f12*t+pi/4);
%  u(1,:)=tmp4;
 