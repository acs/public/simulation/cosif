
% This script calls Simulink models for monolithic model, TD (time domain ITM), DP
% (dynamic-phasors ITM)

%% Select circuit 
sys_parameters.circuit.Num = 1;

%% File location to save results - please adjust 

% %specify subfolder name 
% subFldr = 'resSmlnkCmp_11';
% 
% %write a Note to text file 
% %file location of text file
% flocation = ['equationsValidation/Circuit',num2str(sys_parameters.circuit.Num),'/'];
% fid = fopen(strcat(flocation,'notes.txt'),'a');
% string1 = datestr(now);
% string2 = 'Generate results to compare with Simulink model ';
% % string3 = strcat('Results saved in: ',subFldr); 
% string3 = ['Results saved in: ',subFldr]; %note: [] writes the space after in: , but strcat() does not! 
% fprintf(fid, '\n%s\n%s\n%s\n', string1, string2, string3);
% fclose(fid);
% 
% %flocation for results
% flocation = ['Results/Circuit',num2str(sys_parameters.circuit.Num),'/', subFldr,'/'];

%% Add folder paths
% myPath = genpath(pwd); % returns a path string that includes folderName and multiple levels of subfolders below folderName. The path string does not include folders named private, folders that begin with the @ character (class folders), folders that begin with the + character (package folders), or subfolders within any of these.
% addpath(myPath);


%% Initialization of parameters 
% --------------------------------
sys_parameters.Ts = 50e-6; %simulation/discretization time step
sys_parameters.d = 1; %Tdft = d * Ts; sampling of the DFT (not in use in Simulink model)
sys_parameters.sysFreq = 50.0; %fundamental system frequency 
sys_parameters.T0 = 1/sys_parameters.sysFreq; %20e-3; %fundamental frequency=1/T0 (N = T0/Ts; M = T0/Tdft; N=M*d) -> T0 is fixed for all tests

% --------------------------------
sys_parameters.h_dft = [5 10]; % %= integer >= 1; sampling of the interface H = h_dft * Tdft = h * Ts -> h = h_dft*d 
sys_parameters.dim1_h = sys_parameters.h_dft*sys_parameters.d;  %Sending rate of the interface: h = h_dft * d (H = h_dft*Tdft =  (h_dft * d) * Ts = h * Ts)
sys_parameters.dim2_Ndelay1 = [1 100];  %%delay = Ndelay x Ts (assumption) - delay of "interface" block 
sys_parameters.dim4_DPHrmncs_Tdft =  {-1:1, -3:3};  %NOTE: for Simulink model, max is 10


%% Circuit parameters 
R1 = 1.0; L1 = 1.0e-3;
c1 = 4.0e-3;
R2 = 4.0; L2 = 1.5e-3;    
%Parameters for sys_parameters.circuit.Parameters structure
sys_parameters.circuit.Parameters.R1 = R1;
sys_parameters.circuit.Parameters.L1 = L1;
sys_parameters.circuit.Parameters.c1 = c1;
sys_parameters.circuit.Parameters.R2 = R2;
sys_parameters.circuit.Parameters.L2 = L2;

%%  Time domain simulation with Matlab lsim 

t_final = 2.0; %simulation time
%Generate time and input vectors
%number of external inputs
numIn = 2; % number of external inputs
[t, u] = f_genTimeInputVctrs(numIn, sys_parameters.Ts, t_final);
t_lsim = t';
u_lsim = u'; %note - Simulink and lsim use the following dimensioning: u is t-by-NumInp array, and you use the oposite: u is NumInp-by-t array


%%  Time domain simulation with Simulink (executed by sim command)

%------
% sys_d
% monolithic model (discretizied)
%------ 
%Simulink results
% Simulink model: CircuitNsys_d.mdl 
% Define parameters before you invoke sim command: 
	% t_final - duration of simulation
	% sys_parameters.Ts
	% sys_parameters.circuit  - circuit parameters
	% t_lsim, u_lsim  - See SimConfig.png for Data Import/Export configuration settings
% You can generate the name of .mdl file based on CircuitNum.mdl, where Num is sys_parameters.circuit.Num
mdlName = ['Circuit',num2str(sys_parameters.circuit.Num),'sys_d'];
% Invoke sim command 
simOut = sim(mdlName, 'SaveOutput', 'on');
% Get results (see SimConfig.png for yout and tout labels)
ymdl_d = simOut.get('yout').';  %we transponse because we save in different format
                            %note - Simulink uses the following dimensioning: u is t-by-NumInp matrix, and
                                %you use the oposite: u is NumInp-by-t matrix
tmdl_d = simOut.get('tout').';

%%

%------
% sys_dist_td:     
% time domain ITM
%------
%create cell arrays of empty matrices 
sys_dist_td.ymdl_td{size(sys_parameters.dim1_h,2),size(sys_parameters.dim2_Ndelay1,2)} = [];
sys_dist_td.tmdl_td{size(sys_parameters.dim1_h,2),size(sys_parameters.dim2_Ndelay1,2)} = [];
for dim1Elmnt=1:size(sys_parameters.dim1_h,2)
    for dim2Elmnt=1:size(sys_parameters.dim2_Ndelay1,2)
        %Simulink results
        % Simulink model: CircuitNsys_dist_td.mdl 
        % Define parameters before you invoke sim command: 
        % t_final - duration of simulation
        % sys_parameters.Ts
        % sys_parameters.dim1_h(1,dim1Elmnt) - sampling rate
        % sys_parameters.dim2_Ndelay1(1,dim2Elmnt) - delay
        % sys_parameters.circuit  - circuit parameters
        % You can generate the name of .mdl file based on CircuitNum.mdl, where Num is sys_parameters.circuit.Num
        mdlName = ['Circuit',num2str(sys_parameters.circuit.Num),'sys_dist_td'];
        % Invoke sim command 
        simOut = sim(mdlName, 'SaveOutput', 'on');
        % Get results (see SimConfig.png for yout and tout labels)
        ymdl_td = simOut.get('yout').';  %we transponse because we save in different format
                                    %note - Simulink uses the following dimensioning: u is t-by-NumInp array, and
                                        %you use the oposite: u is NumInp-by-t array
        tmdl_td = simOut.get('tout').';
        %save results
        sys_dist_td.ymdl_td{dim1Elmnt,dim2Elmnt}=ymdl_td;
        sys_dist_td.tmdl_td{dim1Elmnt,dim2Elmnt}=tmdl_td;
    end
end




%%
%------
% sys_dist_dp
% DP ITM
%------
%create cell arrays of empty matrices 
sys_dist_td.ymdl_td{size(sys_parameters.dim1_h,2),size(sys_parameters.dim2_Ndelay1,2), size(sys_parameters.dim4_DPHrmncs_Tdft,2)} = [];
sys_dist_td.tmdl_td{size(sys_parameters.dim1_h,2),size(sys_parameters.dim2_Ndelay1,2), size(sys_parameters.dim4_DPHrmncs_Tdft,2)} = [];
for dim1Elmnt=1:size(sys_parameters.dim1_h,2)
    for dim2Elmnt=1:size(sys_parameters.dim2_Ndelay1,2)
        for dim4Elmnt=1:size(sys_parameters.dim4_DPHrmncs_Tdft,2)
                %Simulink results
                % Simulink model: CircuitNsys_dist_dp.mdl 
                % Define parameters before you invoke sim command: 
                    % t_final - duration of simulation
                    % sys_parameters.Ts
                    % sys_parameters.dim1_h(1,dim1Elmnt) - sampling rate
                    % sys_parameters.dim2_Ndelay1(1,dim2Elmnt) - delay
                    kdp = max(sys_parameters.dim4_DPHrmncs_Tdft{1,dim4Elmnt}); %number of components for DP interface, kdp controls variant of the library block 
                    % sys_parameters.circuit  - circuit parameters
                    % t_lsim, u_lsim  - See SimConfig.png for Data Import/Export configuration settings
                % You can generate the name of .mdl file based on CircuitNum.mdl, where Num is sys_parameters.circuit.Num
                mdlName = ['Circuit',num2str(sys_parameters.circuit.Num),'sys_dist_dp'];
                % Invoke sim command 
                simOut = sim(mdlName, 'SaveOutput', 'on');
                % Get results (see SimConfig.png for yout and tout labels)
                ymdl_dp = simOut.get('yout').';  %we transponse because we save in different format
                                            %note - Simulink uses the following dimensioning: u is t-by-NumInp array, and
                                                %you use the oposite: u is NumInp-by-t array
                tmdl_dp = simOut.get('tout').';
                %save results
                sys_dist_dp.ymdl_dp{dim1Elmnt,dim2Elmnt, dim4Elmnt}=ymdl_dp;
                sys_dist_dp.tmdl_dp{dim1Elmnt,dim2Elmnt, dim4Elmnt}=tmdl_dp;
        end
    end
end


%%  ---------------------------------------------------------------------------------------------- 
% -------------------------------  Delay fixed, number of harmonics changes  ------------------------------------------------------------  
figprefix = 'delayConst_DPhrmncsVar__';

%% Figure: Delay fixed, number of harmonics changes
% NOTE: we do not use here DPHrmncs_Tdft_all (max is 10 in Simulink)
% we do not use delay = 0
% we show only DP interface

%SelectOutput
outY = 3;  %for circuit Number 1: this is current through inducatence 2

%Select parameters for plotting time response
dim1Elmnt = 1; % we keep h = 1;
dim2Elmnt = 2; % 

% Plot time domain 
legendmatrix={}; 
figure; 
     plot(tmdl_d, ymdl_d(outY,:));
     legendmatrix = {legendmatrix{1,:},['Monolithic simulation']};
     hold on;
    for dim4Elmnt=1:(size(sys_parameters.dim4_DPHrmncs_Tdft,2)) % we do not include DPHrmncs_Tdft_all
            plot(sys_dist_dp.tmdl_dp{dim1Elmnt,dim2Elmnt, dim4Elmnt}, sys_dist_dp.ymdl_dp{dim1Elmnt,dim2Elmnt, dim4Elmnt}(outY,:)); %4
            legendmatrix = {legendmatrix{1,:},['Num of DP coefficients =', num2str(max(sys_parameters.dim4_DPHrmncs_Tdft{1,dim4Elmnt}))]};
            hold on;
     end
    
    
legend(legendmatrix);
% titleFig = strcat('Interface current at SS2');
% title(titleFig);
titleFig = ['Time domain response for y(',num2str(outY),'), for INTRF:  h=', num2str(sys_parameters.dim1_h(1,dim1Elmnt)), ',  delay =', num2str(sys_parameters.dim2_Ndelay1(1,dim2Elmnt)*sys_parameters.Ts*1000),'ms'];
title(titleFig);
% xlim([0.4 0.45]);
% ylim([-2 3]);
ylabel('y');
xlabel('time [s]');

if sys_parameters.dim2_Ndelay1(1,dim2Elmnt)*sys_parameters.Ts*1000 < 1
    flname=[figprefix, 'TimeDomain_y',num2str(outY),'_INTRF_h', num2str(sys_parameters.dim1_h(1,dim1Elmnt)), '_Ndelay', num2str(sys_parameters.dim2_Ndelay1(1,dim2Elmnt)*sys_parameters.Ts*1000*1000),'micros'];
else 
    flname=[figprefix, 'y',num2str(outY),'_INTRF_h', num2str(sys_parameters.dim1_h(1,dim1Elmnt)), '_Ndelay', num2str(sys_parameters.dim2_Ndelay1(1,dim2Elmnt)*sys_parameters.Ts*1000),'ms'];
end
savefig(gcf, [pwd,'/Results/',flname]);




%%  ---------------------------------------------------------------------------------------------- 
% -------------------------------   Number of harmonics fixed, delay changes  ------------------------------------------------------------  
figprefix = 'delayVar_DPhrmncsConst__';

%% Figure: Number of DP coefficients fixed, delay changes
% NOTE: we do not use here DPHrmncs_Tdft_all (max is 10 in Simulink)
% we do not use delay = 0
% we show only DP interface

%SelectOutput
outY = 3;  %for circuit Number 1: this is current through inducatence 2

%Select parameters for plotting time response
dim1Elmnt = 1; % we keep h = 1;
dim4Elmnt = 2;

% Plot time domain 
legendmatrix={}; 
figure;
     plot(tmdl_d, ymdl_d(outY,:));
     legendmatrix = {legendmatrix{1,:},['Monolithic simulation']};
     hold on;
    for dim2Elmnt=1:(size(sys_parameters.dim2_Ndelay1,2)) 
            plot(sys_dist_dp.tmdl_dp{dim1Elmnt,dim2Elmnt, dim4Elmnt}, sys_dist_dp.ymdl_dp{dim1Elmnt,dim2Elmnt, dim4Elmnt}(outY,:)); %4
            legendmatrix = {legendmatrix{1,:},['Distributed system with delay: ', num2str(sys_parameters.dim2_Ndelay1(1,dim2Elmnt)*sys_parameters.Ts*1000),'ms']};
            hold on;
    end
    
    
legend(legendmatrix);
% titleFig = strcat('Interface current at SS2');
% title(titleFig);
titleFig = ['Time domain responses for y(',num2str(outY),'), for INTRF:  h=', num2str(sys_parameters.dim1_h(1,dim1Elmnt)), ',Max DPHrmncs=', num2str(max(sys_parameters.dim4_DPHrmncs_Tdft{1,dim4Elmnt}))];
title(titleFig);
% xlim([0.4 0.45]);
ylim([-2 3]);
ylabel('Current [A]');
xlabel('time [s]');

flname=[figprefix, 'y',num2str(outY),'_INTRF_h', num2str(sys_parameters.dim1_h(1,dim1Elmnt)), '_DPHrmncs', num2str(max(sys_parameters.dim4_DPHrmncs_Tdft{1,dim4Elmnt}))];
savefig(gcf, [pwd,'/Results/',flname]);

