#include "cb.h"

#if defined(MATLAB_MEX_FILE) || defined(RTLAB)

static struct cb cb_load(SimStruct *S, int idx)
{
	struct cb cb;

	uint32_T *pos = (uint32_T *) ssGetDWork(S, CB_POS_IDX);

	cb.buffer = (real_T*) ssGetDWork(S, CB_DATA_IDX + idx);
	cb.pos = pos[idx];

	return cb;
}

static void cb_store(struct cb *cb, SimStruct *S, int idx)
{
	uint32_T *pos = (uint32_T *) ssGetDWork(S, CB_POS_IDX);

	pos[idx] = cb->pos;
}

#elif defined(RTDS)

static struct cb cb_load(int idx)
{
	struct cb cb;
	
	cb.buffer = cb_buffer[idx];
	cb.pos = cb_pos[idx];
	
	return cb;
}

static void cb_store(struct cb *cb, int idx)
{
	cb_pos[idx] = cb->pos;
}

#endif

static void cb_init(struct cb *cb)
{
	uint32_T i;

	cb->pos = 0;

	for (i = 0; i < CB_DEPTH; i++)
		cb->buffer[i] = 0.0;
}

static void cb_push(struct cb *cb, real_T val)
{
	cb->buffer[cb->pos] = val;
    
    cb->pos++;
    cb->pos %= CB_DEPTH;
}

static real_T cb_get(struct cb *cb, uint32_T off)
{
	return cb->buffer[(cb->pos + CB_DEPTH - off - 1) % CB_DEPTH];
}

static real_T cb_update(struct cb *cb, real_T val, uint32_T off)
{
	cb_push(cb, val);

	return cb_get(cb, off);
}
