#pragma once

// Timestep
#define TS_USEC		50
#define TS_NSEC		(TS_USEC * 1000)

// Communication period
#define CP_STEPS	1
#define CP_NSEC		(CP_STEPS * TS_NSEC) // in ns

// Observation window
#define OW_STEPS	512 // in number of CP_STEPS

// Length of buffers
// Should be >= OW_STEPS
// Should be >= T_comm_delay / (CP_STEPS * TS)
//    e.x     = 6ms / (10 * 50e-6) = 12
#define CB_DEPTH	1024
#define CB_BUFFERS	12
#define CB_POS_IDX	6
#define CB_DATA_IDX	(CB_POS_IDX + 1)

// Comment out to disable
#define WITH_SIGNAL_ERRORS
#define WITH_SIGNAL_RESIDUALS

#define NPHASES		1

