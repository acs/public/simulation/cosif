#if defined(MATLAB_MEX_FILE)
  #include "tmwtypes.h"
  #include "simstruc_types.h"
#elif !defined(RTDS)
  #include "rtwtypes.h"
#else
typedef int bool;
#endif

#include "cb.h"
#include "ebm_config.h"

/* Passivity Oberserver Flags */
#define FLAG_PO_IF			(1<<0) /* Total simulation time */
#define FLAG_PO_PORT_LOCAL		(1<<1)
#define FLAG_PO_PORT_REMOTE		(1<<2)

#define FLAG_PO_IF_CP			(1<<3) /* For last communication period */
#define FLAG_PO_PORT_LOCAL_CP		(1<<4)
#define FLAG_PO_PORT_REMOTE_CP		(1<<5)

#define FLAG_PO_IF_OW			(1<<6) /* For current observation window */
#define FLAG_PO_PORT_LOCAL_OW		(1<<7)
#define FLAG_PO_PORT_REMOTE_OW		(1<<8)

struct ebm_inputs {
	/* Remote signals are valid. We received new data */
	int recv_ready;

	/* Local timestamp */
	int sec_local;
	int nsec_local;

	/* Remote timestamp and sequence number */
	int sec_remote; /* received from remote side */
	int nsec_remote; /* received from remote side */

	int seqno_remote; /* received from remote side */

	/* Local interface signals */
	const double *v_local;
	const double *i_local;

#if defined(WITH_SIGNAL_RESIDUALS) || defined(WITH_SIGNAL_ERRORS)
	/* Remote interface signals */
	const double *v_remote; /* received from remote side */
	const double *i_remote; /* received from remote side */
#endif

	/* Total energy generated/consumed by the remote port */
	const double *e_gen_remote; /* received from remote side */
	const double *e_con_remote; /* received from remote side */

#ifdef WITH_SIGNAL_ERRORS
	/* Reference signals from monolithic model */
	const double *v_orgn;
	const double *i_orgn;
#endif
};

struct ebm_outputs {
	/* A set of flags. See FLAG_* above */
	int status;

	/* Send enable signal. We should send data to remote now */
	int send_ready;

	/* The EBM calculation is completed. */
	int ebm_ready;

	/* Our local sequence number. Incremented everytime when send_ready == 1 */
	int seqno_local; /* send to remote side */

	/* Delay between remote and local simulator
	 * in number of communication periods */
	int delay;

	/* Re-assignable outputs for scopes */
	double *scopes;

	/* The following signals are _delayed_ and alignned with remote signals */

#ifdef WITH_SIGNAL_RESIDUALS
	/* Delayed instantaneous power */
	double *p_local;
	double *p_remote;

	/* Delayed signal residuals */
	double *res_v;
	double *res_i;

	/* Delayed Power residuals */
	double *res_p;
#endif

	/* Delayed Energy residuals of Interface over 1 COMM_PERIOD */
	double *res_e_cp_active;
	double *res_e_cp_passive;

	/* Delayed Energy resd=iduals of Interface over OW_STEPS * CP_NSEC (for fidelity). */
	double *res_e_ow_active;
	double *res_e_ow_passive;

	/* Delayed Energy resd=iduals of Interface over entire simulation. */
	double *res_e_active;
	double *res_e_passive;

	/* Total energy generated/consumed at local port */
	double *e_gen_local; /* send to remote side */
	double *e_con_local; /* send to remote side */

#ifdef WITH_SIGNAL_ERRORS
	/* Signal errors */
	double *err_v_local;
	double *err_v_remote;

	double *err_i_local;
	double *err_i_remote;
#endif
};

struct ebm_states {
	int seqno_local;
	int seqno_offset;
	int has_seqno_offset;

	double *prev_p;

	/* Accuumlator, Total energy at local port from t=0 */
	double *e_gen;
	double *e_con;

	int *cb_pos;

#ifdef WITH_SIGNAL_RESIDUALS
	/* History of local interface signals */
	struct cb cb_v_local[NPHASES];
	struct cb cb_i_local[NPHASES];
#endif

	/* History of total generated/consumed energy at local port */
	struct cb cb_e_gen_local[NPHASES];
	struct cb cb_e_con_local[NPHASES];

	/* History of total generated/consumed energy at remote port */
	struct cb cb_e_gen_remote[NPHASES];
	struct cb cb_e_con_remote[NPHASES];

	/* History of total active/passive energy at interface */
	struct cb cb_e_if_active[NPHASES];
	struct cb cb_e_if_passive[NPHASES];

#ifdef WITH_SIGNAL_ERRORS
	/* History of reference signals of monolithic model */
	struct cb cb_v_orgn[NPHASES];
	struct cb cb_i_orgn[NPHASES];
#endif
};

#if !defined(RTDS)
extern void ebm_init(struct ebm_states *s);

extern void ebm_step(double dt, const struct ebm_inputs *i, struct ebm_outputs *o, struct ebm_states *s);
#endif