#include "ebm.h"

static void ebm_init(struct ebm_states *s)
{
	int n;

	s->seqno_local = 0;
	s->seqno_offset = 0;
	s->has_seqno_offset = 0;

	for (n = 0; n < NPHASES; n++) {
		s->prev_p[n] = 0;
		s->e_gen[n] = 0;
		s->e_con[n] = 0;

#ifdef WITH_SIGNAL_RESIDUALS
		cb_init(&s->cb_v_local[n]);
		cb_init(&s->cb_i_local[n]);
#endif
		cb_init(&s->cb_e_gen_local[n]);
		cb_init(&s->cb_e_con_local[n]);
		cb_init(&s->cb_e_gen_remote[n]);
		cb_init(&s->cb_e_con_remote[n]);
		cb_init(&s->cb_e_if_active[n]);
		cb_init(&s->cb_e_if_passive[n]);
#ifdef WITH_SIGNAL_ERRORS
		cb_init(&s->cb_v_orgn[n]);
		cb_init(&s->cb_i_orgn[n]);
#endif
	}
}

static void ebm_update_energy(double dt, const struct ebm_inputs *i, struct ebm_states *s)
{
	int n;

	/* Update total energy */
	for (n = 0; n < NPHASES; n++) {
		double p = i->v_local[n] * i->i_local[n];
		double e = dt * (s->prev_p[n] + p) / 2.0;

		if (e >= 0)
			s->e_gen[n] += e;
		else
			s->e_con[n] += e;

		s->prev_p[n] = p;
	}
}

static void ebm_calc_offset(double dt, const struct ebm_inputs *i, struct ebm_states *s)
{
	// Nanoseconds per second
	const int NSPS = 1000000000;

	// Communication Periods per Second
	const int CPPS = NSPS / CP_NSEC;

	int offset_sec = i->sec_local - i->sec_remote;
	int offset_nsec = i->nsec_local - i->nsec_remote;

	if (offset_nsec < 0) {
		offset_sec++;
		offset_nsec += NSPS;
	}

	s->seqno_offset  = i->seqno_remote;
	s->seqno_offset += offset_sec * CPPS;
	s->seqno_offset += offset_nsec / CP_NSEC;
}

static void ebm_calc_metric(double dt, const struct ebm_inputs *i, struct ebm_outputs *o, struct ebm_states *s)
{
	int n;

	double v_orgn, i_orgn;
	double v_local, i_local;
	double e_gen_local, e_con_local;
	double e_if_active, e_if_passive;
	double res_e_incr, res_e_active_incr, res_e_passive_incr;
	double e_gen_local_incr, e_con_local_incr;
	double e_gen_remote_incr, e_con_remote_incr;

	/* Passivity flag will be set if any of the phases is passive */
	o->status = 0;

	o->delay = s->seqno_local - i->seqno_remote - 1;

	for (n = 0; n < NPHASES; n++) {
		o->scopes[0] = 123.456;
		o->scopes[1] = 1234;
		// upto 15

		// TODO: handle lost packets
		cb_push(&s->cb_e_gen_remote[n], i->e_gen_remote[n]);
		cb_push(&s->cb_e_con_remote[n], i->e_con_remote[n]);

#ifdef WITH_SIGNAL_RESIDUALS
		v_local = cb_get(&s->cb_v_local[n], o->delay);
		i_local = cb_get(&s->cb_i_local[n], o->delay);
		
		/* Power Calculation (subsystem) */
		o->p_local[n] = i_local * v_local;
		o->p_remote[n] = i->i_remote[n] * i->v_remote[n];

		/* Signal residuals */
		o->res_v[n] = v_local - i->v_remote[n];
		o->res_i[n] = i_local + i->i_remote[n];

		/* Power residual */
		o->res_p[n] = o->p_local[n] + o->p_remote[n];
#endif

		/* Energy residual at interface */
		e_gen_local_incr  = cb_get(&s->cb_e_gen_local[n], o->delay) - cb_get(&s->cb_e_gen_local[n], o->delay + 1);
		e_con_local_incr  = cb_get(&s->cb_e_con_local[n], o->delay) - cb_get(&s->cb_e_con_local[n], o->delay + 1);
		e_gen_remote_incr = cb_get(&s->cb_e_gen_remote[n], 0) - cb_get(&s->cb_e_gen_remote[n], 1);
		e_con_remote_incr = cb_get(&s->cb_e_con_remote[n], 0) - cb_get(&s->cb_e_con_remote[n], 1);

		res_e_incr = e_gen_local_incr + e_con_local_incr + e_gen_remote_incr + e_con_remote_incr;
		if (res_e_incr < 0) {
			res_e_active_incr = res_e_incr;
			res_e_passive_incr = 0;
		}
		else {
			res_e_active_incr = 0;
			res_e_passive_incr = res_e_incr;
		}

		o->res_e_cp_active[n]  = res_e_active_incr;
		o->res_e_cp_passive[n] = res_e_passive_incr;

		o->res_e_active[n]     = res_e_active_incr  + cb_get(&s->cb_e_if_active[n], 0);
		o->res_e_passive[n]    = res_e_passive_incr + cb_get(&s->cb_e_if_passive[n], 0);

		o->res_e_ow_active[n]  = o->res_e_active[n] - cb_get(&s->cb_e_if_active[n], OW_STEPS);
		o->res_e_ow_passive[n] = o->res_e_passive[n] - cb_get(&s->cb_e_if_passive[n], OW_STEPS);

		cb_push(&s->cb_e_if_active[n],  o->res_e_active[n]);
		cb_push(&s->cb_e_if_passive[n], o->res_e_passive[n]);

        	/* Interface Activity Observer */
		if (o->res_e_active[n] + o->res_e_passive[n] >= 0)
			o->status |= FLAG_PO_IF;
		if (o->res_e_cp_active[n] + o->res_e_cp_passive[n] >= 0)
			o->status |= FLAG_PO_IF_CP;
		if (o->res_e_ow_active[n] + o->res_e_ow_passive[n] >= 0)
			o->status |= FLAG_PO_IF_OW;

		if (e_gen_local_incr + e_con_local_incr < 0)
			o->status |= FLAG_PO_PORT_LOCAL_CP;
		if (e_gen_remote_incr + e_con_remote_incr < 0)
			o->status |= FLAG_PO_PORT_REMOTE_CP;

		// TODO: set remaining FLAG_PO

#ifdef WITH_SIGNAL_ERRORS
		v_orgn = cb_get(&s->cb_v_orgn[n], o->delay);
		i_orgn = cb_get(&s->cb_i_orgn[n], o->delay);

		o->err_v_local[n] = v_local - v_orgn;
		o->err_v_remote[n] = i->v_remote[n] - v_orgn;

		o->err_i_local[n] = i_local - i_orgn;
		o->err_i_remote[n] = i->i_remote[n] + i_orgn;
#endif
	}
}

static int ebm_is_comm_step(const struct ebm_inputs *i)
{
	return i->nsec_local % CP_NSEC == 0;
}

static void ebm_step(double dt, const struct ebm_inputs *i, struct ebm_outputs *o, struct ebm_states *s)
{
	int n;

	ebm_update_energy(dt, i, s);

	o->send_ready = ebm_is_comm_step(i);
	if (o->send_ready) {
		for (n = 0; n < NPHASES; n++) {
			o->e_gen_local[n] = s->e_gen[n];
			o->e_con_local[n] = s->e_con[n];

#ifdef WITH_SIGNAL_RESIDUALS
			cb_push(&s->cb_v_local[n], i->v_local[n]);
			cb_push(&s->cb_i_local[n], i->i_local[n]);
#endif

#ifdef WITH_SIGNAL_ERRORS
			cb_push(&s->cb_v_orgn[n], i->v_orgn[n]);
			cb_push(&s->cb_i_orgn[n], i->i_orgn[n]);
#endif
			/* Save local energy to history buffer */
			cb_push(&s->cb_e_gen_local[n], o->e_gen_local[n]);
			cb_push(&s->cb_e_con_local[n], o->e_con_local[n]);
		}
        
        o->seqno_local = s->seqno_local;
        
        s->seqno_local++;
	}

	/* Received new signals from remote */
	if (i->recv_ready) {
		/* If we know already our time offset to remote
		 * We will calc residuals and EBM
		 */
		if (!s->has_seqno_offset) {
			ebm_calc_offset(dt, i, s);
        		s->has_seqno_offset = 1;
        	}

		ebm_calc_metric(dt, i, o, s);
	}

	/* Send signals to remote */
	if (o->send_ready) {
		
	}

	o->ebm_ready = i->recv_ready;
}
