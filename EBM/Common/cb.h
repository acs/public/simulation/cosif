#ifndef _CB_H_
#define _CB_H_

#if defined(MATLAB_MEX_FILE) || defined(RTLAB)
  #include "simstruc.h"
  #include "tmwtypes.h"
#else
  typedef unsigned int uint32_T;
  typedef double real_T;
#endif

#include "ebm_config.h"

struct cb {
	real_T *buffer;

	uint32_T pos;
};

#if defined(MATLAB_MEX_FILE) || defined(RTLAB)
struct cb cb_load(SimStruct *S, int idx);
void cb_store(struct cb *cb, SimStruct *S, int idx);
#endif

#if !defined(RTDS)
void cb_init(struct cb *cb);

/* Adds new value to history buffer. */
void cb_push(struct cb *cb, real_T val);

/* Get value from history buffer which is _off_ slots old. */
real_T cb_get(struct cb *cb, uint32_T off);

/* Inserts new value into history buffer and gets old value at the same time */
real_T cb_update(struct cb *cb, real_T val, uint32_T off);
#endif

#endif /* _CB_H_ */
