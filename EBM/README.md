# CBuilder / S-function version of Energy Based Metric for GD-RTS

## Compilation

### CBuilder

1. Copy all files from the C-Builder and Common directories to `C:/RTDS_USER/BIN/CMODEL_SOURCE`
2. Copy the component definition file `ebm_v2.def` to `C:/RTDS_USER/ULIB`
2. Open the RSCAD CBuilder application
3. Load the component definition file `ebm_v2.def`
4. Compile the component

### Simulink

1. Create a new folder called `ebm`.
2. Add the new folder to your MATLAB search path: `path(path, '/path/to/ebm')`
3. Copy all files from the Common and S-Function directories into the new directory
4. From within the `ebm` directory, run: `build` in the MATLAB prompt
5. Open the Simulink library `CoSiIF_ebm_lib.slx` to copy the block to your model
