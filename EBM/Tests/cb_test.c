#include <stdio.h>

#include "cb.h"

int main(int argc, char **argv)
{
	struct cb cb;

	real_T b[100];

	cb.buffer = b;

	cb_init(&cb, 100);

	for (int i = 0; i < 200; i++) {
		cb_push(&cb, i);

		printf("%d = %f\n", i, cb_get(&cb, 10));
	}

	return 0;
}
