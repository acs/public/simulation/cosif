#define S_FUNCTION_LEVEL	2
#define S_FUNCTION_NAME		ebm_v2

#define SFUNWIZ_GENERATE_TLC	1
#define SOURCEFILES		"__SFB__"
#define PANELINDEX		8
#define USE_SIMSTRUCT		0
#define SHOW_COMPILE_STEPS	0
#define CREATE_DEBUG_MEXFILE	0
#define SAVE_CODE_ONLY		0
#define SFUNWIZ_REVISION	3.0

#include "simstruc.h"
#include "ebm.h"

struct output_port {
	const char *name;
	int width;
	int data_type;
	int complex;
	size_t offset;
} output_ports[] = {
	{ "status", 		1, SS_UINT32, COMPLEX_NO, offsetof(struct ebm_outputs, status) },
	{ "send_ready", 	1, SS_UINT32, COMPLEX_NO, offsetof(struct ebm_outputs, send_ready) },
	{ "ebm_ready", 		1, SS_UINT32, COMPLEX_NO, offsetof(struct ebm_outputs, ebm_ready) },
	{ "seqno_local", 	1, SS_UINT32, COMPLEX_NO, offsetof(struct ebm_outputs, seqno_local) },
	{ "delay",	 	1, SS_UINT32, COMPLEX_NO, offsetof(struct ebm_outputs, delay) },
	{ "scopes",		16, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, scopes) },
	{ "p_local", 		NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, p_local) },
	{ "p_remote", 		NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, p_remote) },
	{ "res_v", 		NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, res_v) },
	{ "res_i", 		NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, res_i) },
	{ "res_p", 		NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, res_p) },
	{ "res_e_cp_active",	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, res_e_cp_active) },
	{ "res_e_cp_passive",	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, res_e_cp_passive) },
	{ "res_e_ow_active",	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, res_e_ow_active) },
	{ "res_e_ow_passive",	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, res_e_ow_passive) },
	{ "res_e_active",	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, res_e_active) },
	{ "res_e_passive",	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, res_e_passive) },
	{ "e_gen_local",	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, e_gen_local) },
	{ "e_con_local",	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, e_con_local) },
#ifdef WITH_ERROR_SIGNALS
	{ "err_v_local", 	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, err_v_local) },
	{ "err_v_remote", 	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, err_v_remote) },
	{ "err_i_local", 	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, err_i_local) },
	{ "err_i_remote", 	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_outputs, err_i_remote) },
#endif
};

struct input_port {
	const char *name;
	int width;
	int data_type;
	int complex;
	int direct_feedthrough;
	int required_continous;
	size_t offset;
} input_ports[] = {
	{ "recv_ready",		1, SS_UINT32, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, recv_ready) },
	{ "sec_local",		1, SS_UINT32, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, sec_local) },
	{ "nsec_local", 	1, SS_UINT32, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, nsec_local) },
	{ "sec_remote", 	1, SS_UINT32, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, sec_remote) },
	{ "nsec_remote", 	1, SS_UINT32, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, nsec_remote) },
	{ "seqno_remote",	1, SS_UINT32, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, seqno_remote) },
	{ "v_local",		NPHASES, SS_DOUBLE, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, v_local) },
	{ "i_local", 		NPHASES, SS_DOUBLE, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, i_local) },
	{ "v_remote", 		NPHASES, SS_DOUBLE, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, v_remote) },
	{ "i_remote", 		NPHASES, SS_DOUBLE, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, i_remote) },
	{ "e_gen_remote",	NPHASES, SS_DOUBLE, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, e_gen_remote) },
	{ "e_con_remote",	NPHASES, SS_DOUBLE, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, e_con_remote) },
#ifdef WITH_ERROR_SIGNALS
	{ "v_orgn",		NPHASES, SS_DOUBLE, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, v_orgn) },
	{ "i_orgn",		NPHASES, SS_DOUBLE, COMPLEX_NO, 1, 1, offsetof(struct ebm_inputs, i_orgn) },
#endif
};

struct dwork {
	const char *name;
	int width;
	int data_type;
	int complex;
	size_t offset;
} dwork[] = {
	{ "seqno_local",	1, SS_UINT32, COMPLEX_NO, offsetof(struct ebm_states, seqno_local) },
	{ "seqno_offset",	1, SS_UINT32, COMPLEX_NO, offsetof(struct ebm_states, seqno_offset) },
	{ "has_seqno_off",	1, SS_UINT32, COMPLEX_NO, offsetof(struct ebm_states, has_seqno_offset) },
	{ "prev_p",	  	NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, prev_p) },
	{ "e_gen",		NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, e_gen) },
	{ "e_con",		NPHASES, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, e_con) },
	{ "cb_pos", 		CB_BUFFERS, SS_UINT32, COMPLEX_NO, offsetof(struct ebm_states, cb_pos) },
	{ "cb_i_local", 	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_i_local) },
	{ "cb_v_local", 	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_v_local) },
	{ "cb_e_gen_local",	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_e_gen_local) },
	{ "cb_e_con_local",	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_e_con_local) },
	{ "cb_e_gen_remote",	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_e_gen_remote) },
	{ "cb_e_con_remote",	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_e_con_remote) },
	{ "cb_e_if_active",	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_e_if_active) },
	{ "cb_e_if_passive",	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_e_if_passive) },
#ifdef WITH_ERROR_SIGNALS
	{ "cb_i_orgn",  	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_i_orgn) },
	{ "cb_v_orgn",  	CB_DEPTH, SS_DOUBLE, COMPLEX_NO, offsetof(struct ebm_states, cb_v_orgn) },
#endif
};

#define IS_PARAM_DOUBLE(pVal) (mxIsNumeric(pVal) && !mxIsLogical(pVal) &&\
!mxIsEmpty(pVal) && !mxIsSparse(pVal) && !mxIsComplex(pVal) && mxIsDouble(pVal))


/*====================*
 * S-function methods *
 *====================*/

#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS)  && defined(MATLAB_MEX_FILE)
static void mdlCheckParameters(SimStruct *S)
{
	const mxArray *pVal0 = ssGetSFcnParam(S,0);

	if ( mxGetNumberOfElements(ssGetSFcnParam(S,0)) != 1 || !IS_PARAM_DOUBLE(pVal0)) {
		ssSetErrorStatus(S, "Parameter to S-function must be a scalar");
		return;
	}
	else if (mxGetPr(ssGetSFcnParam(S,0))[0] < 0) {
		ssSetErrorStatus(S, "Parameter to S-function must be non-negative");
		return;
	}
}
#endif

static void mdlInitializeSizes(SimStruct *S)
{
	int i;
	int num_input_ports = sizeof(input_ports)/sizeof(input_ports[0]);
	int num_output_ports = sizeof(output_ports)/sizeof(output_ports[0]);
	int num_dwork = sizeof(dwork)/sizeof(dwork[0]);

	DECL_AND_INIT_DIMSINFO(inputDimsInfo);
	DECL_AND_INIT_DIMSINFO(outputDimsInfo);

	/* Params */
	ssSetNumSFcnParams(S, 1);
	if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
		return; /* Parameter mismatch will be reported by Simulink */
	}

	ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);

	ssSetNumContStates(S, 0);
	ssSetNumDiscStates(S, 0);

	ssSetNumDWork(S, num_dwork);

	if (!ssSetNumInputPorts(S, num_input_ports))
		return;

	if (!ssSetNumOutputPorts(S, num_output_ports))
		return;

	for (i = 0; i < num_input_ports; i++) {
		struct input_port p = input_ports[i];

		ssSetInputPortWidth(S, i, p.width);
		ssSetInputPortDataType(S, i, p.data_type);
		ssSetInputPortComplexSignal(S, i, p.complex);
		ssSetInputPortDirectFeedThrough(S, i, p.direct_feedthrough);
		ssSetInputPortRequiredContiguous(S, i, p.required_continous); /*direct input signal access*/
	}

	for (i = 0; i < num_output_ports; i++) {
		struct output_port p = output_ports[i];

		ssSetOutputPortWidth(S, i, p.width);
		ssSetOutputPortDataType(S, i, p.data_type);
		ssSetOutputPortComplexSignal(S, i, p.complex);
	}

	for (i = 0; i < num_dwork; i++) {
		struct dwork p = dwork[i];

		ssSetDWorkWidth(S, i, p.width);
		ssSetDWorkDataType(S, i, p.data_type);
		ssSetDWorkName(S, i, p.name);
		ssSetDWorkComplexSignal(S, i, p.complex);
	}

	ssSetNumPWork(S, 0);
	ssSetNumSampleTimes(S, 1);
	ssSetNumRWork(S, 0);
	ssSetNumIWork(S, 0);
	ssSetNumModes(S, 0);
	ssSetNumNonsampledZCs(S, 0);

	//ssSetSimulinkVersionGeneratedIn(S, "9.1");

	/* Take care when specifying exception free code - see sfuntmpl_doc.c */
	ssSetOptions(S, (SS_OPTION_EXCEPTION_FREE_CODE |
					 SS_OPTION_USE_TLC_WITH_ACCELERATOR |
					 SS_OPTION_WORKS_WITH_CODE_REUSE));
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
	ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
	ssSetModelReferenceSampleTimeDefaultInheritance(S);
	ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_SET_INPUT_PORT_DATA_TYPE
static void mdlSetInputPortDataType(SimStruct *S, int port, DTypeId dType)
{
	ssSetInputPortDataType(S, 0, dType);
}

#define MDL_SET_OUTPUT_PORT_DATA_TYPE
static void mdlSetOutputPortDataType(SimStruct *S, int port, DTypeId dType)
{
	ssSetOutputPortDataType(S, 0, dType);
}

#define MDL_SET_DEFAULT_PORT_DATA_TYPES
static void mdlSetDefaultPortDataTypes(SimStruct *S)
{
	ssSetInputPortDataType(S, 0, SS_DOUBLE);
	ssSetOutputPortDataType(S, 0, SS_DOUBLE);
}

static void ebm_load(SimStruct *S, struct ebm_inputs *i, struct ebm_outputs *o, struct ebm_states *s)
{
	int n, p;

	/* Load Inputs */
	if (i) {
		p = 0;
		i->recv_ready   	= *(uint32_T *)ssGetInputPortSignal(S, p++);
		i->sec_local		= *(uint32_T *)ssGetInputPortSignal(S, p++);
		i->nsec_local   	= *(uint32_T *)ssGetInputPortSignal(S, p++);
		i->sec_remote   	= *(uint32_T *)ssGetInputPortSignal(S, p++);
		i->nsec_remote  	= *(uint32_T *)ssGetInputPortSignal(S, p++);
		i->seqno_remote 	= *(uint32_T *)ssGetInputPortSignal(S, p++);

		i->v_local		= ssGetInputPortRealSignal(S, p++);
		i->i_local		= ssGetInputPortRealSignal(S, p++);
		i->v_remote		= ssGetInputPortRealSignal(S, p++);
		i->i_remote		= ssGetInputPortRealSignal(S, p++);
		i->e_gen_remote		= ssGetInputPortRealSignal(S, p++);
		i->e_con_remote		= ssGetInputPortRealSignal(S, p++);
#ifdef WITH_ERROR_SIGNALS
		i->v_orgn		= ssGetInputPortRealSignal(S, p++);
		i->i_orgn		= ssGetInputPortRealSignal(S, p++);
#endif
	}

	/* Load output pointers */
	if (o) {
		p = 5; // Index of first vector output
		o->scopes		= ssGetOutputPortRealSignal(S, p++);
		o->p_local		= ssGetOutputPortRealSignal(S, p++);
		o->p_remote		= ssGetOutputPortRealSignal(S, p++);
		o->res_v		= ssGetOutputPortRealSignal(S, p++);
		o->res_i		= ssGetOutputPortRealSignal(S, p++);
		o->res_p		= ssGetOutputPortRealSignal(S, p++);
		o->res_e_cp_active	= ssGetOutputPortRealSignal(S, p++);
		o->res_e_cp_passive	= ssGetOutputPortRealSignal(S, p++);
		o->res_e_ow_active	= ssGetOutputPortRealSignal(S, p++);
		o->res_e_ow_passive	= ssGetOutputPortRealSignal(S, p++);
		o->res_e_active		= ssGetOutputPortRealSignal(S, p++);
		o->res_e_passive	= ssGetOutputPortRealSignal(S, p++);
		o->e_gen_local		= ssGetOutputPortRealSignal(S, p++);
		o->e_con_local		= ssGetOutputPortRealSignal(S, p++);
 #ifdef WITH_ERROR_SIGNALS
		o->err_v_local		= ssGetOutputPortRealSignal(S, p++);
		o->err_v_remote		= ssGetOutputPortRealSignal(S, p++);
		o->err_i_local		= ssGetOutputPortRealSignal(S, p++);
		o->err_i_remote		= ssGetOutputPortRealSignal(S, p++);
 #endif
	}

	/* Load states */
	if (s) {
		p = 0;
		s->seqno_local		= *(uint32_T*) ssGetDWork(S, p++);
		s->seqno_offset 	= *(uint32_T*) ssGetDWork(S, p++);
		s->has_seqno_offset 	= *(uint32_T*) ssGetDWork(S, p++);
		s->prev_p		= (real_T*) ssGetDWork(S, p++);
		s->e_gen		= (real_T*) ssGetDWork(S, p++);
		s->e_con		= (real_T*) ssGetDWork(S, p++);

		/* Load Buffers */
		p = 0;
		for (n = 0; n < NPHASES; n++) {
			s->cb_v_local[n]	= cb_load(S, p++);
			s->cb_i_local[n]	= cb_load(S, p++);
			s->cb_e_gen_local[n]	= cb_load(S, p++);
			s->cb_e_con_local[n]	= cb_load(S, p++);
			s->cb_e_gen_remote[n]	= cb_load(S, p++);
			s->cb_e_con_remote[n]	= cb_load(S, p++);
			s->cb_e_if_active[n]	= cb_load(S, p++);
			s->cb_e_if_passive[n]	= cb_load(S, p++);
#ifdef WITH_ERROR_SIGNALS
			s->cb_v_orgn[n]		= cb_load(S, p++);
			s->cb_i_orgn[n]		= cb_load(S, p++);
#endif
		}
	}
}

static void ebm_store(SimStruct *S, struct ebm_inputs *i, struct ebm_outputs *o, struct ebm_states *s)
{
	int n, p;

	/* Store Buffers */
	if (s) {
		p = 0;
		for (n = 0; n < NPHASES; n++) {
			cb_store(&s->cb_v_local[n], S, p++);
			cb_store(&s->cb_i_local[n], S, p++);
			cb_store(&s->cb_e_gen_local[n], S, p++);
			cb_store(&s->cb_e_con_local[n], S, p++);
			cb_store(&s->cb_e_gen_remote[n], S, p++);
			cb_store(&s->cb_e_con_remote[n], S, p++);
			cb_store(&s->cb_e_if_active[n], S, p++);
			cb_store(&s->cb_e_if_passive[n], S, p++);
#ifdef WITH_ERROR_SIGNALS
			cb_store(&s->cb_v_orgn[n], S, p++);
			cb_store(&s->cb_i_orgn[n], S, p++);
#endif
		}

		/* Save states */
		p = 0;
		*(uint32_T*) ssGetDWork(S, p++) = s->seqno_local;
		*(uint32_T*) ssGetDWork(S, p++) = s->seqno_offset;
		*(uint32_T*) ssGetDWork(S, p++) = s->has_seqno_offset;
	}

	/* Save Outputs */
	if (o) {
		p = 0;
		*(uint32_T *) ssGetOutputPortSignal(S, p++) = o->status;
		*(uint32_T *) ssGetOutputPortSignal(S, p++) = o->send_ready;
		*(uint32_T *) ssGetOutputPortSignal(S, p++) = o->ebm_ready;
		*(uint32_T *) ssGetOutputPortSignal(S, p++) = o->seqno_local;
		*(uint32_T *) ssGetOutputPortSignal(S, p++) = o->delay;
	}
}

#define MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START)
/* Function: mdlStart =======================================================
 * Abstract:
 *	This function is called once at start of model execution. If you
 *	have states that should be initialized once, this is the place
 *	to do it.
 */
static void mdlStart(SimStruct *S)
{
	struct ebm_states s;

	ebm_load(S, NULL, NULL, &s);
	ebm_init(&s);
	ebm_store(S, NULL, NULL, &s);
}
#endif /*  MDL_START */

/* Function: mdlOutputs =======================================================
 *
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
	struct ebm_inputs i;
	struct ebm_outputs o;
	struct ebm_states s;

	real_T *dt = mxGetPr(ssGetSFcnParam(S, 0));

	ebm_load(S, &i, &o, &s);
	ebm_step(*dt, &i, &o, &s);
	ebm_store(S, &i, &o, &s);
}

/* Function: mdlTerminate =====================================================
 * Abstract:
 *	In this function, you should perform any actions that are necessary
 *	at the termination of a simulation.  For example, if memory was
 *	allocated in mdlStart, this is the place to free it.
 */
static void mdlTerminate(SimStruct *S)
{

}

#ifdef  MATLAB_MEX_FILE	/* Is this file being compiled as a MEX-file? */
  #include "simulink.c"	  /* MEX-file interface mechanism */
#else
  #include "cg_sfun.h"	   /* Code generation registration function */
#endif
