systems = dir('../**/*.slx');
versions = { 'R2014A', 'R2011B' };

for i = 1:numel(systems)
  system = systems(i);

  if system.isdir
    continue;
  end

  [filepath,name,ext] = fileparts([system.folder filesep system.name]);

  cd(filepath);

  load_system(name);

  for j = 1:numel(versions)
    version = char(versions(j));
    dirname = lower(version);
    newname = strcat(dirname, filesep, name);

    mkdir(dirname);
    save_system(name, newname, 'ExportToVersion', version, 'OverwriteIfChangedOnDisk', true);
  end

  close_system(name);
end
