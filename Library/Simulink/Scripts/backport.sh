#!/bin/bash

MATLAB=/Applications/MATLAB_R2017a.app/bin/matlab

SCRIPT=${1:-backport.m}
SD=$(pwd)

$MATLAB -nodisplay -nosplash -nodesktop -sd "${SD}" -r "run ${SCRIPT}; exit;"
