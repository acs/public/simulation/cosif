# <img src="Documentation/images/cosif.png" width=40 /> CoSiF: Co-Simulation Interface Algorithms

CoSif is a collection of **co**-**s**imulation **I**nter**f**ace Algorithms (IA) for (geographically) distributed real-time simulation.
It has been developed by the Institute for Automation of Complex Power Systems at RWTH Aachen University with the goal to simplify the execution of distributed co-simulations by using using re-usable library blocks within the simulations models.

We aim to provide a set of tested and interoperable implementations of IA targeted at the following platforms:

- OPAL-RT eMEGASIM (MATLAB Simulink)
- RTDS (RSCAD)
- VILLASnode / DPsim

At the moment we aim to provide interface algorithms for the following domains:

- HVDC-VSC
- Vrms / Freq / Phase
- PQ
- Dynamic Phasor ITM

## Documentation

The documentation for CoSiF is not yet complete. The current working state can be found in the Git repo:

- [CoSiF Documentation](https://git.rwth-aachen.de/acs/public/simulation/cosif/tree/master/Documentation)

For further question please consider joining our Slack channel: [FEIN e.V. Slack](https://join.slack.com/t/feinev/shared_invite/enQtNTE1NjY5MTg5NTY4LWM4MWI5ZTVkNDgzZTgyNmY5NWY2N2M3MjdjYzQxY2E0MmRlNjBkYTc3ODNlMDliY2M5YzllNjE4YTY3ODBjM2M).

## Copyright

2018-2020, Institute for Automation of Complex Power Systems, EONERC, RWTH Aachen University

## License

This project is released under the terms of the [GPL version 3](https://dpsim.fein-aachen.org/doc/development/sphinx/Copying.html).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](https://www.fein-aachen.org/img/logos/eonerc.png)](http://www.acs.eonerc.rwth-aachen.de)

- Marija Stevic <mstevic@eonerc.rwth-aachen.de>
- Steffen Vogel <stvogel@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
